var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id
    this.color = color
    this.modelo = modelo
    this.ubicacion = ubicacion
}

Bicicleta.prototype.toString = function() {
    return `id: ${this.id} | color: ${this.color}`
}

Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici)
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
    if (aBici) {
        return aBici
    } else {
        throw new error(`no existe un a bicicleta con el id ${aBiciId}`)
    }
}

Bicicleta.removeById = function(aBiciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1)
            break
        }
    }
}

Bicicleta.allBicis = []
let a = new Bicicleta(1, 'rojo', 'urbana', [6.1663109, -75.6169491])
let b = new Bicicleta(2, 'blanca', 'urbana', [6.15481, -75.5842202])

Bicicleta.add(a)
Bicicleta.add(b)

module.exports = Bicicleta