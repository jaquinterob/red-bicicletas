var mymap = L.map('main_map').setView([6.1663109, -75.6169491], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
}).addTo(mymap);

// L.marker([6.1663109, -75.6169491]).addTo(mymap);
// L.marker([6.15481, -75.5842202]).addTo(mymap);
// L.marker([6.1515078, -75.6169884, 17]).addTo(mymap);

$.ajax({
    dataType: 'json',
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.Bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
        });
    }
})